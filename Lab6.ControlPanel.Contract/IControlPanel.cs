﻿using System;
using System.Windows;

namespace Lab6.ControlPanel.Contract
{
    public interface IControlPanel
    {
        Window Window { get; }
    }
}
