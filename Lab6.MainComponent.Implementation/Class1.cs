﻿using Lab6.Display.Contract;
using Lab6.MainComponent.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6.MainComponent.Implementation
{
    public class Układ_Wejściowy : IUkładWejściowy
    {
        private IDisplay display;

        public IDisplay Display
        {
            get { return display; }
            set { display = value; }
        }

        private int id;

        public Układ_Wejściowy(IDisplay display)
        {
            id = 0;
            this.display = display;
        }

        public void Działam()
        {
            Console.WriteLine("Układ wejściowy działa");
        }

        public void Wyświetlam()
        {
            throw new NotImplementedException();
        }
    }
}
