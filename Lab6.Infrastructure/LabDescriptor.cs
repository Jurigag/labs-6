﻿using Lab6.Container;
using Lab6.ControlPanel.Contract;
using Lab6.ControlPanel.Implementation;
using Lab6.MainComponent.Contract;
using Lab6.MainComponent.Implementation;
using PK.Container;
using System;
using System.Reflection;

namespace Lab6.Infrastructure
{
    public struct LabDescriptor
    {
        #region P1

        public static Func<IContainer> ContainerFactory = () => new PKContainer();

        public static Assembly ControlPanelSpec = Assembly.GetAssembly(typeof(IControlPanel));
        public static Assembly ControlPanelImpl = Assembly.GetAssembly(typeof(ControlPanelImpl));

        public static Assembly MainComponentSpec = Assembly.GetAssembly(typeof(IUkładWejściowy));
        public static Assembly MainComponentImpl = Assembly.GetAssembly(typeof(Układ_Wejściowy));

        #endregion
    }
}
