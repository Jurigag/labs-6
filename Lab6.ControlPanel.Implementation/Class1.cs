﻿using Lab6.ControlPanel.Contract;
using Lab6.MainComponent.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6.ControlPanel.Implementation
{
    public class ControlPanelImpl:IControlPanel
    {
        IUkładWejściowy układ;

        public ControlPanelImpl(IUkładWejściowy a)
        {
            this.układ = a;
        }

        public System.Windows.Window Window
        {
            get { return new Window1(); }
        }
    }
}
